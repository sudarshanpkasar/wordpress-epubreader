<?php
/*
Plugin Name: ePub reader
Plugin URI: http://www.anshuverma.com
Description: embed ePub reader to wordpress post or page. 
Version: 1.0.0
Author: Anshu Verma
Author URI: http://www.anshuverma.com/
License: GNU General Public License v2.0
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Copyright 2013  Anshu Verma (anshu@anshuverma.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
    
*/


  /**
   * For the header.
   */
   
function epubreader_header(){
echo '<script src="' .plugins_url('/build/epub.min.js', __FILE__). '"></script>'; 
echo '<script src="' .plugins_url('/build/libs/zip.min.js', __FILE__). '"></script>'; 
?>
        <style type="text/css">
          #epubwrapper {
            width: 100%;
          }

          #wrapper {
            width: 100%;
            height: 480px;
            overflow: hidden;
            border: 1px solid #ccc;
            margin: 20px auto;
            background: #fff;
            border-radius: 0 5px 5px 0;
            float: left;
            overflow-x:scroll;
            overflow-y:scroll;
            -webkit-overflow-scrolling: touch; //adding this line will enable inertial scrolling
          }

          #area {
            width: 100%;
            height: 480px;
            margin: 0 auto;
            -moz-box-shadow:      inset 10px 0 20px rgba(0,0,0,.1);
            -webkit-box-shadow:   inset 10px 0 20px rgba(0,0,0,.1);
            box-shadow:           inset 10px 0 20px rgba(0,0,0,.1);
            overflow-x:scroll;
            overflow-y:scroll;
            -webkit-overflow-scrolling: touch; //adding this line will enable inertial scrolling
          }

          #prev {
            width: 10px;
            float: left;
            margin-left: 10px;
          }

          #next {
            width: 10px;
            float: right;
            margin-right: 10px;
          }

          .arrow {
            top: 50%;
            font-size: 64px;
            color: #E2E2E2;
            font-family: arial, sans-serif;
            font-weight: bold;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
          }

          .arrow:hover {
            color: #777;
          }
           
          .arrow:active {
            color: #000;
          }
        </style>

        <script>
            EPUBJS.filePath = "<?php echo plugins_url('/build/libs/', __FILE__); ?>";
        </script>
<?php
}

function epub_reader( $atts ) {
  extract(shortcode_atts(array(
       'link' => '',
   ), $atts));
?>
        <div id="epubwrapper">
          <div id="prev" onclick="Book.prevPage();" class="arrow">‹</div>
          <div id="wrapper">
            <div id="area"></div>
          </div>
          <div id="next" onclick="Book.nextPage();" class="arrow">›</div>
        </div>

<script>
    "use strict";
    var Book = ePub("<?php echo $link; ?>", { 
      height: 480,
      spreads : false,
      fixedLayout : true

     });
    Book.renderTo("area").then(function(){
              Book.setStyle("padding", "0 10px");
    });
</script>
<?php
}
add_shortcode( 'epubreader', 'epub_reader' );
add_action('wp_head', epubreader_header, 1);
?>